﻿CREATE TABLE [dbo].[ProyectoInvestigacion]
(
	[InvestigadorId]		BIGINT         NOT NULL,
	[Id]					BIGINT         IDENTITY (1, 1) NOT NULL,
	[Nombre]				NVARCHAR (100) NOT NULL,
	[FechaInicio]			date		NOT NULL,
	[FechaFinalizacion]		date		NOT NULL,
	[IdGrupoInvestigacion]	BIGINT      NOT NULL,
	PRIMARY KEY CLUSTERED ([InvestigadorId] ASC, [Id] ASC),
	FOREIGN KEY ([IdGrupoInvestigacion]) REFERENCES [dbo].[GrupoInvestigacion] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY ([InvestigadorId]) REFERENCES [dbo].[Investigador] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,


)
