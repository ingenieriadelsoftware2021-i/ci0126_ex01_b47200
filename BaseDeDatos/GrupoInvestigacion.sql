﻿CREATE TABLE [dbo].[GrupoInvestigacion]
(
	[Id]									BIGINT         IDENTITY (1, 1) NOT NULL,
    [Nombre]								NVARCHAR (100) NOT NULL,
	[FechaCreacion]							date		NOT NULL,
	[Descripcion]							NVARCHAR (100) NOT NULL,
	[IdCoordinador]							BIGINT         NOT NULL,
	[IdAreaInvestigacion]					BIGINT         NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	FOREIGN KEY ([IdCoordinador]) REFERENCES [dbo].[Coordinador] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY ([IdAreaInvestigacion]) REFERENCES [dbo].[AreaInvestigacion] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,

)
