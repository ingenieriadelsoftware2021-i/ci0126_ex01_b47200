﻿CREATE TABLE [dbo].[Investigador]
(
	[Id]					BIGINT         NOT NULL,
	[IdGrado]				BIGINT         NOT NULL,
	[IdGrupoInvestigacion]	BIGINT		   NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	FOREIGN KEY ([IdGrado]) REFERENCES [dbo].[Grado] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT [FK_IdPersona] FOREIGN KEY ([Id]) REFERENCES [dbo].[Autor] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
	
	CONSTRAINT [FK_GrupoInvestigacion] FOREIGN KEY ([IdGrupoInvestigacion]) REFERENCES [dbo].[GrupoInvestigacion] ([Id]),

)
