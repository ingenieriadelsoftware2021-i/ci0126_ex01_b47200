﻿CREATE TABLE [dbo].[Grado]
(
	[Id]              BigInt     IDENTITY (1, 1) NOT NULL,
    [Nombre]          NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Grado] PRIMARY KEY CLUSTERED ([Id] ASC),
)
