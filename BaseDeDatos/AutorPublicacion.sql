﻿CREATE TABLE [dbo].[AutorPublicacion]
(
	[Doi]					NVARCHAR (100)		NOT NULL,
	[IdAutor]					BigInt     IDENTITY (1, 1) NOT NULL,
	[Orden]					Int NOT NULL,

	PRIMARY KEY CLUSTERED ([Doi] ASC, [IdAutor] ASC),
	FOREIGN KEY ([Doi]) REFERENCES [dbo].[Publicacion] ([Doi]),
	FOREIGN KEY ([IdAutor]) REFERENCES [dbo].[Autor] ([Id]) ,
)
