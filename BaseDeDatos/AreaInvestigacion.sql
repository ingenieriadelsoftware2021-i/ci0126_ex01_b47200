﻿CREATE TABLE [dbo].[AreaInvestigacion]
(
	[Id]		BIGINT         IDENTITY (1, 1) NOT NULL,
	[Nombre]								NVARCHAR (100) NOT NULL,
	[Descripcion]							NVARCHAR (100) NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
)
