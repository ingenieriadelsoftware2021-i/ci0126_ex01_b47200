﻿CREATE TABLE [dbo].[Coordinador]
(
	[Id]					BIGINT         NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_IdPersonaCoordinador] FOREIGN KEY ([Id]) REFERENCES [dbo].[Autor] ([Id]) ON DELETE NO ACTION  ,
)
