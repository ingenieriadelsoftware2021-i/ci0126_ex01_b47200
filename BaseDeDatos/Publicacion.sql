﻿CREATE TABLE [dbo].[Publicacion]
(
	[Doi]					NVARCHAR (100)		NOT NULL,
	[Nombre]				NVARCHAR (100)		NOT NULL,
	[Resumen]				NVARCHAR (100)		NOT NULL,
	[Tipo]					BIT					NOT NULL,
	[Referencia]			NVARCHAR (100)	    NOT NULL,
	PRIMARY KEY CLUSTERED ([Doi] ASC)
)
