﻿CREATE TABLE [dbo].[Autor]
(
	[Id]              BigInt     IDENTITY (1, 1) NOT NULL,
    [Nombre]          NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED ([Id] ASC),
);

