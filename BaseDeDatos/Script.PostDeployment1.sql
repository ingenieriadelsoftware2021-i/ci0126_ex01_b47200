﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


SET IDENTITY_INSERT [Autor]  ON
MERGE INTO [Autor] AS Target
USING (VALUES 
        (1,'Juan Valverde'),
        (2,'Hansel Calderon'),
        (3,'Herson Mena'),
        (4,'Eddy Ruiz'),
        (5,'Silvia Aguilar')
)
AS Source ([Id], [Nombre]) 
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id], [Nombre]) 
VALUES ([Id], [Nombre]); 
SET IDENTITY_INSERT [Autor]  OFF

MERGE INTO [Coordinador] AS Target
USING (VALUES 
        (1),
        (4)
)
AS Source ([Id])
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id]) 
VALUES ([Id]); 

SET IDENTITY_INSERT [Grado] ON
MERGE INTO [Grado] AS Target
USING (VALUES 
        (1,'Doctor'),
        (2,'Master')
)
AS Source ([Id],[Nombre])
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id],[Nombre]) 
VALUES ([Id],[Nombre]); 
SET IDENTITY_INSERT [Grado] OFF


MERGE INTO [Investigador] AS Target
USING (VALUES 
        (2,1,1),
        (3,2,1),
        (5,2,1)
)
AS Source ([Id],[IdGrado],[IdGrupoInvestigacion])
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id],[IdGrado],[IdGrupoInvestigacion]) 
VALUES ([Id],[IdGrado],[IdGrupoInvestigacion]); 

SET IDENTITY_INSERT [AreaInvestigacion]  ON
MERGE INTO [AreaInvestigacion] AS Target
USING (VALUES 
        (1,'Area de Informatica','Area que desarrolla software funcional'),
        (2,'Area de Biologia','Area que desarrolla vacunas')
)
AS Source ([Id],[Nombre],[Descripcion]) 
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id],[Nombre],[Descripcion]) 
VALUES ([Id],[Nombre],[Descripcion]); 
SET IDENTITY_INSERT [AreaInvestigacion]  OFF

SET IDENTITY_INSERT [GrupoInvestigacion] ON
MERGE INTO [GrupoInvestigacion] AS Target
USING (VALUES 
        (1,'GrupoLLeno','2020-06-02','Un grupo de prueba que tiene investigadores',1,1),
        (2,'GrupoVacio','2021-06-26','Un grupo de prueba que no tiene investigadores',4,2)
)
AS Source ([Id],[Nombre],[FechaCreacion],[Descripcion],[IdCoordinador],[IdAreaInvestigacion]) 
ON Target.[Id] = Source.[Id]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Id],[Nombre],[FechaCreacion],[Descripcion],[IdCoordinador],[IdAreaInvestigacion]) 
VALUES ([Id],[Nombre],[FechaCreacion],[Descripcion],[IdCoordinador],[IdAreaInvestigacion]); 

SET IDENTITY_INSERT [GrupoInvestigacion] OFF

MERGE INTO [Publicacion] AS Target
USING (VALUES 
        ('jde2jupi','Publicacion1','Primera publicacion de grupo',1,'Programacion'), --1 Es revista 
        ('jde2jupisdakfj','Publicacion2','Segunda publicacion de grupo',0,'Base de datos') -- 0 conferencia
)
AS Source ([Doi],[Nombre],[Resumen],[Tipo],[Referencia]) 
ON Target.[Doi] = Source.[Doi]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Doi],[Nombre],[Resumen],[Tipo],[Referencia]) 
VALUES ([Doi],[Nombre],[Resumen],[Tipo],[Referencia]); 



SET IDENTITY_INSERT [AutorPublicacion] ON
MERGE INTO [AutorPublicacion] AS Target
USING (VALUES 
        ('jde2jupi',2,1), 
        ('jde2jupi',3,2), 
        ('jde2jupisdakfj',2,1),
        ('jde2jupisdakfj',3,2) 
)
AS Source ([Doi],[IdAutor],[Orden]) 
ON Target.[Doi] = Source.[Doi] AND Target.[IdAutor] = Source.[IdAutor]
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([Doi],[IdAutor],[Orden]) 
VALUES ([Doi],[IdAutor],[Orden]); 

SET IDENTITY_INSERT [AutorPublicacion] OFF