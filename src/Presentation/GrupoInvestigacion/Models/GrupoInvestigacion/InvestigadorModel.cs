namespace Examen1.Presentation.GrupoInvestigacion.Models.GrupoInvestigacion
{
    public class InvestigadorModel
    {
        public string? Nombre { get; set; }
        public string? ProyectoInvestigacion { get; set; }
    }
}
