using Examen1.Application.GrupoInvestigacion;
using Examen1.Application.GrupoInvestigacion.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace Lab2.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services)
        {
            services.AddTransient<IGrupoService, GrupoService>();
            return services;
        }

    }
}
