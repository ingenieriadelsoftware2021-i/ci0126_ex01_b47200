using System.Collections.Generic;
using System.Threading.Tasks;
using Examen1.Domain.GrupoInvestigacion.DTOs;
using Examen1.Domain.GrupoInvestigacion.Entities;

namespace Examen1.Application.GrupoInvestigacion
{
    public interface IGrupoService
    {
        Task<IEnumerable<GrupoDTO>> ObtenerGruposInvestigacion();
        Task<Grupo?> ObtenerGrupoPorId(long id);
        Task AsignarInvestigadroAGrupo(Grupo grupo, Investigador investigador);
        Task EliminarInvestigadorAGrupo(Grupo grupo, Investigador investigador);
    }
}
