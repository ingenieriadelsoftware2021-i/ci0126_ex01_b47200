using System.Collections.Generic;
using System.Threading.Tasks;
using Examen1.Domain.GrupoInvestigacion.DTOs;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Examen1.Domain.GrupoInvestigacion.Repositories;

namespace Examen1.Application.GrupoInvestigacion.Implementations
{
    internal class GrupoService : IGrupoService
    {
        private readonly IGrupoRepository _grupoRepository;

        public GrupoService(IGrupoRepository grupoRepository)
        {
            _grupoRepository = grupoRepository;
        }

        public async Task AsignarInvestigadroAGrupo(Grupo grupo, Investigador investigador)
        {
            await _grupoRepository.AgregarInvestigador(investigador);
        }

        public async Task EliminarInvestigadorAGrupo(Grupo grupo, Investigador investigador)
        {
            investigador.sePuedeEliminar();
            await _grupoRepository.eliminarInvestigador(investigador);
        }

        public async Task<Grupo> ObtenerGrupoPorId(long id)
        {
            return await _grupoRepository.ObtenerGrupoInvestigacion(id);
        }

        public async Task<IEnumerable<GrupoDTO>> ObtenerGruposInvestigacion()
        {
            return await _grupoRepository.ObtenerTodosLosGrupos();
        }
    }
}
