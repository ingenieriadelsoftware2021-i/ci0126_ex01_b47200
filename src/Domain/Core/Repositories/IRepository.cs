using System.Threading.Tasks;
using Examen1.Domain.Core.Entities;

namespace Examen1.Domain.Core.Repositories
{
    public interface IRepository<TAggregateRoot> where TAggregateRoot : AggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}