﻿using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.GrupoInvestigacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1.Domain.Core.Entities
{
    public class Autor : Entity
    {
        public RequiredString Nombre { get; set; }
        public Autor(RequiredString nombre)
        {
            Nombre = nombre;
        }

        private readonly List<AutorPublicacion> _autorPublicacion;
        public IReadOnlyCollection<AutorPublicacion> AutorPublicacion;
        public Autor()
        {
        }
    }
}
