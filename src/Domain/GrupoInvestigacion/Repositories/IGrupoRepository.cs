using System.Collections.Generic;
using System.Threading.Tasks;
using Examen1.Domain.Core.Repositories;
using Examen1.Domain.GrupoInvestigacion.DTOs;
using Examen1.Domain.GrupoInvestigacion.Entities;

namespace Examen1.Domain.GrupoInvestigacion.Repositories
{
    public interface IGrupoRepository : IRepository<Grupo>
    {
        Task SaveAsync(Grupo team);
        Task<IEnumerable<GrupoDTO>> ObtenerTodosLosGrupos();
        Task<Grupo?> ObtenerGrupoInvestigacion(long id);
        Task BorrarInvestigador(Investigador investigador);
        Task AgregarInvestigador(Investigador investigador);
        Task ActualizarInvestigador(Investigador investigador);
        Task eliminarInvestigador(Investigador investigador);
    }
}
