using Examen1.Domain.Core.ValueObjects;

namespace Examen1.Domain.GrupoInvestigacion.DTOs
{
    public class GrupoDTO
    {
        public long Id { get; }
        public RequiredString Name { get; }

        public GrupoDTO(long id, RequiredString name)
        {
            Id = id;
            Name = name;
        }
    }
}
