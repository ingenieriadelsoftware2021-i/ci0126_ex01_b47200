using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Examen1.Domain.Core.Entities;
using Examen1.Domain.Core.Exceptions;
using Examen1.Domain.Core.ValueObjects;

namespace Examen1.Domain.GrupoInvestigacion.Entities
{
    public class Grupo : AggregateRoot
    {

        private  List<Investigador> _investigadors = null!;

        public RequiredString Nombre { get; }
        public DateTime FechaCreacion { get; }
        public RequiredString Descripcion { get; }
        public IReadOnlyCollection<Investigador> Investigadores; 

        public Grupo(RequiredString nombre)
        {
            _investigadors = new List<Investigador>();
            Nombre = nombre;
        }

        // for EFCore
        private Grupo()
        {
            Nombre = null!;
            _investigadors = null!;
            Descripcion = null!;
            FechaCreacion = DateTime.Now;
        }

        public void eliminarInvestigador(Investigador investigador)
        {
            if (Investigadores.Exists(p => p == investigador))
            {
                _investigadors = (List<Investigador>)Investigadores;
                _investigadors.Remove(investigador);
                Investigadores = _investigadors.AsReadOnly();
                //player.AssignTeam(null);
            }
        }

        public void AgregarInvestigador(Investigador investigador)
        {
            if (_investigadors.Exists(p => p == investigador))
                throw new DomainException("El investigador ya forma parte del grupo de investigacion.");

            _investigadors.Add(investigador);
            investigador.AsignarGrupo(this);
        }

        public void RemovePlayerFromRoster(Investigador investigador)
        {
            if (_investigadors.Exists(p => p == investigador))
            {
                _investigadors.Remove(investigador);
                investigador.AsignarGrupo(null);
            }
        }
    }
}