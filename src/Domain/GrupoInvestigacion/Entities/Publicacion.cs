using System;
using System.Collections.Generic;
using Examen1.Domain.Core.Entities;
using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.GrupoInvestigacion.Entities;
using LanguageExt;

namespace Examen1.Domain.GrupoInvestigacion.Entities
{
    public class Publicacion
    {
        public RequiredString Doi { get; set; }
        public RequiredString Nombre { get; set; }
        public string Resumen { get; set; }
        public bool Tipo { get; set; }
        public string Referencia { get; set; }
        private readonly List<AutorPublicacion> _autorPublicacion;
        public IReadOnlyCollection<AutorPublicacion> AutorPublicacion;

        private Publicacion()
        {
        }
    }
}