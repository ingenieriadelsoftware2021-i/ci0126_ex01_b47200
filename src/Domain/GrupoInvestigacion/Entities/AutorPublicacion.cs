﻿using Examen1.Domain.Core.Entities;
using Examen1.Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1.Domain.GrupoInvestigacion.Entities
{
    public class AutorPublicacion
    {
        public RequiredString Doi { get; set; }
        public long IdAutor { get; set; }
        public int Orden { get; set; }
        public Autor Autor { get; set; }
        public Publicacion Publicacion { get; set; }
    }
}
