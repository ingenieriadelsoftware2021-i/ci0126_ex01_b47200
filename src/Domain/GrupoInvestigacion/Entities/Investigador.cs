using Examen1.Domain.Core.Entities;
using Examen1.Domain.Core.Exceptions;
using Examen1.Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Examen1.Domain.GrupoInvestigacion.Entities
{
    public class Investigador : Autor
    {
        public Grupo? Grupo { get; private set; }
        public long IdGrado { get; private set; }
        public long IdGrupoInvestigacion { get; private set; }
        public Investigador(RequiredString nombre, Grupo? grupo, List<AutorPublicacion> proyectos)
            : base(nombre)
        {
            Grupo = grupo;
            AutorPublicacion = proyectos.AsReadOnly();
        }

        // for EFCore
        public Investigador(RequiredString nombre, long idGrupoInvestigacion)
             : base(nombre)
        {
            IdGrupoInvestigacion = idGrupoInvestigacion;
            IdGrado = 2;
        }

        public void sePuedeEliminar()
        {
            if (AutorPublicacion.Count() > 0) {
                throw new DomainException(
                   "No se puede eliminar investigadores que ya tienen proyectos");
            };
        }

        private Investigador()
        {
        }

        public void AsignarGrupo(Grupo? grupo)
        {
            Grupo = grupo;
        }
        private void modificarGrado(long idGrado)
        {
            this.IdGrado = idGrado;
        }
    }
}