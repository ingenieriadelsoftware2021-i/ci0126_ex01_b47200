using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen1.Domain.Core.Repositories;
using Examen1.Domain.GrupoInvestigacion.DTOs;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Examen1.Domain.GrupoInvestigacion.Repositories;
using Examen1.Infrastructure.Core;
using Examen1.Infrastructure.GrupoInvestigacion;
using Microsoft.EntityFrameworkCore;

namespace Examen1.Infrastructure.GrupoInvestigacion.Repositories
{
    internal class GrupoRepository : IGrupoRepository
    {
        private readonly GrupoDbContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;

        public GrupoRepository(GrupoDbContext unitOfWork)
        {
            _dbContext = unitOfWork;
        }

        public async Task SaveAsync(Grupo grupo)
        {
            _dbContext.Update(grupo);

            await _dbContext.SaveEntitiesAsync();
        }

        public async Task<IEnumerable<GrupoDTO>> ObtenerTodosLosGrupos()
        {
            
            return await _dbContext.Grupos
                .Select(t => new GrupoDTO(t.Id, t.Nombre))
                .ToListAsync();
           
        }

        public async Task<Grupo?> ObtenerGrupoInvestigacion(long id)
        {
            Grupo? grupo = await _dbContext.Grupos
               .Include(t => t.Investigadores)
               .ThenInclude(t => t.AutorPublicacion)
                    .ThenInclude(t => t.Publicacion)
               .FirstOrDefaultAsync(t => t.Id == id);
            return grupo;
        }

        public Task BorrarInvestigador(Investigador investigador)
        {
            throw new System.NotImplementedException();
        }

        public async Task AgregarInvestigador(Investigador investigador)
        {
            _dbContext.Add(investigador);
            await _dbContext.SaveEntitiesAsync();
        }

        public  Task ActualizarInvestigador(Investigador investigador)
        {
            throw new System.NotImplementedException();
        }

        public async Task eliminarInvestigador(Investigador investigador)
        {
            _dbContext.Remove(investigador);
            await _dbContext.SaveEntitiesAsync();
        }
    }
}
