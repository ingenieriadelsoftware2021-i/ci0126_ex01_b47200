using Examen1.Domain.Core.Entities;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Examen1.Infrastructure.Core;
using Examen1.Infrastructure.Core.EntityMapping;
using Examen1.Infrastructure.GrupoInvestigacion.EntityMappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Examen1.Infrastructure.GrupoInvestigacion
{
    public class GrupoDbContext : ApplicationDbContext
    {

        public GrupoDbContext(DbContextOptions options, ILogger<GrupoDbContext> logger) : base(options, logger)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(message => Debug.WriteLine(message));
        }
        public DbSet<Autor> Autors { get; set; }
        public DbSet<Grupo> Grupos { get; set; }
        public DbSet<Investigador> Players { get; set; } 
        public DbSet<Publicacion> ProyectoInvestigacions { get; set; }
        public DbSet<AutorPublicacion> AutorPublicaciones { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new GrupoMap());
            modelBuilder.ApplyConfiguration(new AutorMap());
            modelBuilder.ApplyConfiguration(new InvestigadorMap());
            modelBuilder.ApplyConfiguration(new PublicacionMap());
            modelBuilder.ApplyConfiguration(new AutorPublicacionMap());
        }
    }
}
