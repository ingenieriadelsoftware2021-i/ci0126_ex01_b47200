using Examen1.Domain.Core.Helpers;
using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Examen1.Infrastructure.GrupoInvestigacion.EntityMappings
{
    public class PublicacionMap : IEntityTypeConfiguration<Publicacion>
    { 

        public void Configure(EntityTypeBuilder<Publicacion> builder)
        {
            builder.ToTable("Publicacion");
            builder.HasKey(e => new { e.Doi});
            builder.Property(p => p.Nombre)
                 .HasConversion(
                     r => r.Value,
                     s => RequiredString.TryCreate(s).Success()).HasColumnName("Nombre");
            builder.Property(p => p.Doi)
                .HasConversion(
                    r => r.Value,
                    s => RequiredString.TryCreate(s).Success()).HasColumnName("Doi");
            builder.Property(p => p.Resumen).HasColumnName("Resumen");
            builder.Property(p => p.Referencia).HasColumnName("Referencia"); 
            builder.Property(p => p.Tipo).HasColumnName("Tipo");
            builder.Ignore(e => e.AutorPublicacion);
        }
    }
}
