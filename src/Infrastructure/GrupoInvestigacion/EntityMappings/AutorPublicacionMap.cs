﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.Core.Helpers;

namespace Examen1.Infrastructure.GrupoInvestigacion.EntityMappings
{

    public class AutorPublicacionMap : IEntityTypeConfiguration<AutorPublicacion>
    {
        public void Configure(EntityTypeBuilder<AutorPublicacion> builder)
        {
            builder.ToTable("AutorPublicacion");

            builder.HasKey(e => new {
                e.IdAutor,
                e.Doi
            });

            builder.Property(p => p.Doi)
               .HasConversion(
                   r => r.Value,
                   s => RequiredString.TryCreate(s).Success()).HasColumnName("Doi");

            
            builder.Property(p => p.Orden).HasColumnName("Orden");

            builder.HasOne(d => d.Autor)
               .WithMany(p => p.AutorPublicacion)
               .HasForeignKey(d => new { d.IdAutor }).IsRequired();

            builder.HasOne(d => d.Publicacion)
               .WithMany(p => p.AutorPublicacion)
               .HasForeignKey(d => d.Doi);
        }
    }
}
