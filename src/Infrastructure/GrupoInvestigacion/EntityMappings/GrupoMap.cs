using Examen1.Domain.Core.Helpers;
using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Examen1.Infrastructure.GrupoInvestigacion.EntityMappings
{
    public class GrupoMap : IEntityTypeConfiguration<Grupo>
    {
        public void Configure(EntityTypeBuilder<Grupo> builder)
        {
            builder.ToTable("GrupoInvestigacion");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nombre)
                .HasConversion(
                    r => r.Value,
                    s => RequiredString.TryCreate(s).Success()).HasColumnName("Nombre");

            builder.Property(p => p.FechaCreacion).HasColumnName("FechaCreacion");

           builder.Property(p => p.Descripcion)
                .HasConversion(
                    r => r.Value,
                    s => RequiredString.TryCreate(s).Success()).HasColumnName("Descripcion");
            
            builder.HasMany(t => t.Investigadores)
                 .WithOne(p => p.Grupo!);
        }
    }
}
