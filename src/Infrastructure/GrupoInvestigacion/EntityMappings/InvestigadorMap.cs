using Examen1.Domain.Core.Helpers;
using Examen1.Domain.Core.ValueObjects;
using Examen1.Domain.GrupoInvestigacion.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Examen1.Infrastructure.GrupoInvestigacion.EntityMappings
{
    public class InvestigadorMap : IEntityTypeConfiguration<Investigador>
    {


        public void Configure(EntityTypeBuilder<Investigador> builder)
        {
            builder.ToTable("Investigador");

            builder.Property(p => p.IdGrado).HasColumnName("IdGrado");

            builder.HasOne(e => e.Grupo)
                  .WithMany(c => c.Investigadores)
                  .HasForeignKey(e => e.IdGrupoInvestigacion);

        }
    }
}
