using System.Net.Mime;
using Examen1.Domain.GrupoInvestigacion.Repositories;
using Examen1.Infrastructure.GrupoInvestigacion;
using Examen1.Infrastructure.GrupoInvestigacion.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Lab2.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructureLayer(
            this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<GrupoDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddScoped<IGrupoRepository, GrupoRepository>();

            return services;
        }
    }
}
