﻿using Examen1.Domain.Core.Entities;
using Examen1.Domain.Core.Helpers;
using Examen1.Domain.Core.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1.Infrastructure.Core.EntityMapping
{
    public class AutorMap : IEntityTypeConfiguration<Autor>
    {


        public void Configure(EntityTypeBuilder<Autor> builder)
        {
            builder.ToTable("Autor");

            builder.HasKey(p => p.Id);

            builder.Property(t => t.Nombre)
                 .HasConversion(r => r.Value,
                     s => RequiredString.TryCreate(s).Success()).HasColumnName("Nombre");

            builder.HasMany(e => e.AutorPublicacion)
                  .WithOne(c => c.Autor);
        }
    }
}

